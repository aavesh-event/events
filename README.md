# Aavesh Events

<p align="center">
<img src="./public/images/favicon.png" alt="Planner" />
</p>
<p align="center">
Aavesh Events. 
<br />
</p>

# Development

To clone copy the following command in your terminal and start development.

```sh
git clone https://akshatkhosya@bitbucket.org/aavesh-event/events.git
```

cd into the directory

```sh
cd events
```
For the first time make your branch with your name
```sh
git checkout -b <newBranch> main
```
After this push your branch
```sh
git push origin <newBranch>
```
Now you can work on your code
```sh

```
Install all the dependencies
```sh
npm i
```

Run the project in development mode

```sh
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# Author & Contributors

-   [Akshat Mittal](https://akshatmittal61.github.io/portfolio)
-   [Akshat Khosya](https://github.com/akshat-khosya)

## References and Libraries Used

-   [MUI](https://mui.com/)
-   [AOS](https://github.com/michalsnik/aos)
